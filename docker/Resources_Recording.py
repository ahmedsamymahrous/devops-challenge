import os.path
import psutil
import datetime
import csv


# Create CSV file for every resource ( CPU / MEM / DISK )
def csv_file(resource_record, file_name):
    timestamp = datetime.datetime.now()
    file_exists = os.path.isfile(file_name)

    with open (file_name, 'a') as csvfile:
        headers = ["Utilization%", "Timestamp"]
        writer = csv.DictWriter(csvfile, delimiter=',', lineterminator='\n', fieldnames=headers)

        if not file_exists:
            writer.writeheader()  # If the file doesn't exist, write a header

        writer.writerow({"Utilization%" : resource_record , "Timestamp" : timestamp})



# CPU Utilization
def cpu():
    # Calling psutil.cpu_precent() for 5 seconds
    # Because, the CPU utilization is calculated over a period of time. So, it is recommended to provide a time interval.
    cpu_util = "{:.2f}".format(psutil.cpu_percent(5))
    file_name = "/opt/CPU.csv"
    csv_file(cpu_util, file_name)


# Free memory percentage
def mem():
    free_memory = "{:.2f}".format(( psutil.virtual_memory().free / psutil.virtual_memory().total ) * 100 )
    file_name = "/opt/MEM.csv"
    csv_file(free_memory, file_name)
	

# Root disk available space by percentage
def disk():
    path = '/'
    available_space = "{:.2f}".format(100 - psutil.disk_usage(path).percent)
    file_name = "/opt/DISK.csv"
    csv_file(available_space, file_name)


cpu()
mem()
disk()

